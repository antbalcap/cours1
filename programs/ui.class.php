<?php

require_once dirname(__FILE__).'functions.php';

$App = courstest_App();

$App->includeUi();

class courstest_Ui extends app_Ui
{
    
    public function __construct(){
        $this->setApp(courstest_App());
    }

    
    public function includeDossierTest(){
        require_once FUNC_COURSTEST_UI_PATH . 'dossuier.ui.php';
    }
    
    
}