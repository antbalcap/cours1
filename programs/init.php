<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License

// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by CapWelton ({@link https://www.capwelton.com})
 */

require_once dirname(__FILE__) . '/functions.php';

function courstest_upgrade($version_base, $version_ini)
{    
    
    $functionalities = new bab_functionalities();
    require_once dirname(__FILE__) . '/courstest.php';
    $addonName = "courstest";
    $addon = bab_getAddonInfosInstance($addonName);
    $addonPhpPath = $addon->getPhpPath();
    
    
    if ($functionalities->registerClass('Func_App_CoursTest', $addonPhpPath . 'courstest.php')) {
        echo(bab_toHtml('Functionality "Func_App_CoursTest" registered.'));
    }
    
    $App = courstest_App();
    
    
    
    // synchronisation de la base de donnees et des set 
    $mysqlBackend = new ORM_MySqlBackend(bab_getDB());
    $synchronize = new bab_synchronizeSql();
    
    $sets  = array(
        "DossierSet"
    );
    
    $sql = "";
    
    foreach($sets as $s){
        $tempSet = $App->$s();
        $sql .= $mysqlBackend->setToSql($tempSet) . "\n";
    }
    
    $synchronize->fromSqlString($sql);
    
    
    $addon->removeAllEventListeners();
    
    $addonPhpPath = $addon->getPhpPath();
    
    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'coursTest_onSiteMapItems', 'init.php');
    @bab_functionality::includefile('PortletBackend');
    return true;
}


/**
 * Creation des liens sur la gauche 
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function coursTest_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';
    
    bab_functionality::includefile('Icons');
    $App = courstest_App();
    
    $mergeAddonsMenu = bab_Registry::get('/core/sitemap/mergeAddonsMenu', false);
    if ($mergeAddonsMenu) {
        $baseSitemapPosition = array('root', 'DGAll', 'babUser');
        $rootSitemapPosition = array('root', 'DGAll', 'babUser', 'courstest_root');
    } else {
        $baseSitemapPosition = array('root', 'DGAll', 'babUser', 'babUserSectionAddons');
        $rootSitemapPosition = array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'courstest_root');
    }
    
    $item = $event->createItem('courstest_root');
    $item->setLabel($App->translate('Cours test'));
    $item->setPosition($baseSitemapPosition);
    $item->addIconClassname('apps-addon-courstest');
    $event->addFolder($item);
    
    var_dump($App->Controller()->Dossier()->displayList());
    $item = $event->createItem('courstest_Dossier');
    $item->setLabel($App->translate('Dossier'));
    $item->setLink($App->Controller()->Dossier()->displayList()->url());
    $item->setPosition($rootSitemapPosition);
    $item->addIconClassname(Func_Icons::ACTIONS_ARCHIVE_CREATE);
    $event->addFolder($item);
    
}

function courstest_onDeleteAddon()
{
    return true;
}