<?php

include_once 'base.php';


$App = bab_functionality::get('App/courstest');

$App->includeController();


class courstest_controller extends app_Controller{
    
    /**
     * {@inheritDoc}
     * @see app_Controller::getControllerTg()
     */
    protected function getControllerTg()
    {
        return 'addon/courstest/main';
    }
    
    
    /**
     * Get object name to use in URL from the controller classname
     * @param string $classname
     * @return string
     */
    protected function getObjectName($classname)
    {
        $prefix = strlen('courstest_Ctrl');
        return strtolower(substr($classname, $prefix));
    }
    
    
    /**
     * 
     * @param boolean $proxy
     * @return coursTest_ctrlDossier
     */
    public function Dossier($proxy = true){
        require_once FUNC_COURSTEST_CTRL_PATH .'/dossier.ctrl.php';
        return $this->App()->ControllerProxy("courstest_ctrlDossier",$proxy);
    }
    
}