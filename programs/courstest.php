<?php

/*
 * Declaration du module et de se configuraiton
 *
 */

include_once(bab_getAddonInfosInstance("LibApp")->getPhpPath()."app.php");
$addon_coursTest = bab_getAddonInfosInstance('courstest');
if (!$addon_coursTest) {
    return;
}

var_dump($addon_coursTest->getPhpPath());
define('FUNC_COURSTEST_PHP_PATH', $addon_coursTest->getPhpPath());
define('FUNC_COURSTEST_SET_PATH', FUNC_COURSTEST_PHP_PATH);
define('FUNC_COURSTEST_CTRL_PATH', FUNC_COURSTEST_PHP_PATH);
define('FUNC_COURSTEST_UI_PATH', FUNC_COURSTEST_PHP_PATH);




/**
 *
 *
 * @method DossierSet coursTest_DossierSet
 * @author antoi
 *
 */
class Func_App_CoursTest extends Func_App{
    
    public function __construct(){
        parent::__construct();
        $this->addonName = "courstest";
        $this->addonPrefix = "courstest";
        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/courstest/main';
        $this->recordSetPath = FUNC_COURSTEST_SET_PATH;
        
    }
    
    /**
     * @return string
     *
     */
    public function getDescription()
    {
        return 'Module de test';
    }
    
    
    
    /**
     * Permet d'appeler tout les set automatiquement
     * attention il faut qu'ils soient tous appeles pareil
     * fichier : nom.class.php
     * class : nomModule_nomSetClassName
     *
     * cela permet de pas a avoir a declarer chaque set un a un
     *
     * @param string $name
     * @param mixed $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        switch (true) {
            
            case substr($name, -strlen('SetClassName')) === 'SetClassName':
                $recordSetName = substr($name, 0, strlen($name) - strlen('ClassName'));
                return $this->classPrefix . $recordSetName;
            case substr($name, -strlen('Set')) === 'Set':
                $className = $this->classPrefix . $name;
                $recordSet = new $className($this);
                return $recordSet;
        }
        return null;
    }
    
    
    /**
     *
     * @return Func_Translate_Gettext
     */
    private static function getTranslator()
    {
        static $translator = null;
        if (!isset($translator)) {
            $translator = bab_functionality::get('Translate/Gettext', false);
            $translator->setAddonName('courstest');
        }
        
        return $translator;
    }
    
    
    /**
     * {@inheritDoc}
     * @see Func_App::setTranslateLanguage()
     */
    public function setTranslateLanguage($language)
    {
        parent::setTranslateLanguage($language);
        $translator = self::getTranslator();
        $translator->setLanguage($language);
    }
    
    
    /**
     * Translates the string.
     *
     * @param string $str           Text to translate or singular form
     * @param string $str_plurals   Plurals form string
     * @param int $number           Number of items for plurals
     *
     * @return string
     */
    public function translate($str, $str_plurals = null, $number = null)
    {
        $translation = $str;
        
        $translator = self::getTranslator();
        $translation = $translator->translate($str, $str_plurals, $number);
        if ($translation === $str) {
            $translation = parent::translate($str, $str_plurals, $number);
        }
        
        return $translation;
    }
    
    
    /**
     * Includes Controller class definition.
     *
     * inclue le fichier du controlleur du module, qui gere tout les controlleur
     *
     */
    public function includeController()
    {
        parent::includeController();
        require_once FUNC_COURSTEST_PHP_PATH . 'courstest.ctrl.php';
    }
    
    /**
     *  Construit et renvoie le controlleur principal du module
     * @return coursTest_Controller
     */
    function Controller()
    {
        self::includeController();
        return new coursTest_Controller($this);
    }
    
    
    /**
     * Include class courstest_Ui
     *
     * inclue le fichier qui contient tout les outils d'ui
     *
     *
     */
    public function includeUi()
    {
        parent::includeUi();
        require_once FUNC_COURSTEST_PHP_PATH . 'ui.class.php';
    }
    
    /**
     * The crmbtpebook_Ui object propose an access to all ui attachments and ui objects (widgets)
     *
     * @return courstest_Ui
     */
    public function Ui()
    {
        $this->includeUi();
        return bab_getInstance('courstest_Ui');
    }
    
}

if (courstest_App()) {
    spl_autoload_register(array(courstest_App(), 'loadObject'));
}

